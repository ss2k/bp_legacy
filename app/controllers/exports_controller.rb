class ExportsController < ApplicationController

  def index
    @merchants = Merchant.all.to_json({include: :addresses})
    render json: @merchants
  end
end