class Merchant < ActiveRecord::Base
  has_many :stores, foreign_key: :FKID
  has_many :contacts, foreign_key: :FKIDMerchant
  has_many :addresses, foreign_key: :FKID
  has_many :printers, through: :stores
  has_many :pos, through: :stores
end
