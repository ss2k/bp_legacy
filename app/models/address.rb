class Address < ActiveRecord::Base
  self.table_name = "Address"

  has_one :store, foreign_key: :ID
  belongs_to :merchant, foreign_key: :FKID
end
