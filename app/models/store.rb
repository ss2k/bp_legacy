class Store < ActiveRecord::Base
  self.table_name = "Store"

  belongs_to :merchant, foreign_key: :FKID
  has_one :address, foreign_key: :ID
  has_many :printers, foreign_key: :FKIDStore
  has_many :pos, foreign_key: :FKIDStore
end
