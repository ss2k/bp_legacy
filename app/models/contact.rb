class Contact < ActiveRecord::Base
  self.table_name = "Contact"

  belongs_to :merchant, foreign_key: :FKIDMerchant
end
