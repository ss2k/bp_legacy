class Printer < ActiveRecord::Base
  belongs_to :store, foreign_key: :FKIDStore
  belongs_to :merchant, foreign_key: :FKIDMerchant
end
